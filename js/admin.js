(function ($, Drupal) {

	function loopButtons() {
		// Loop over all buttons to style ones we need to style
		$('.modal').find('.btn, .button, button').each(function() {
			if(!$(this).parents('.layout-wrapper').length) {
				$(this).addClass('modal-button');
			}
		});
	}
	// Force backdrop on Bootstrap modals
	$(window).on('show.bs.modal', function (event) {

		if($(event.target).find('.bs_nav-tabs').length) {
			$(event.target).addClass('modal--has-tabs');
		}

		loopButtons();

		// Trigger resize for correct modal position
		$(window).trigger('resize');

		// Replace backdrop by our own system to allow multiple modals
		$(event.target).data('bs.modal').options.backdrop = false;
	});

	// Prevent CKEditor bug due to fixed container and scrolled down window
	$(window).on('shown.bs.modal', function (event) {
		window.scrollTop = $(window).scrollTop();
		window.scrollTo(0, 0);
	});
	$(window).on('hide.bs.modal', function (event) {
		window.scrollTo(0, window.scrollTop);
	});


	$(window).on('dialog:afterclose', function (event) {
		// Rerender modals after a modal has been closed to fix wrong scroll offsets
		$('.modal').hide()

		setTimeout(function() {
			$('.modal').show(0);
		}, 50);
	});

	// Observe the body for added children
	const observer = new MutationObserver((mutationsList) => {
		mutationsList.forEach((mutation) => {

			// If the child is the ckeditor link modal and there is a modal,
			// append it to the modal
			if($(mutation.addedNodes).is($('.ck-body-wrapper')) && $('.modal').length) {
				$('.ck-body-wrapper').appendTo($('.modal'));
			}
		});
	});
	observer.observe(document.body, {childList: true});

	Drupal.behaviors.VbAdmin = {
		attach: function (context, settings) {
			loopButtons();

			
			
			// Hide consecutive messages
			// if($('[data-drupal-messages]').length > 1) {
			// 	$('[data-drupal-messages]').slice(1).hide();
			// }

			// Open tabs menu when clicking More
			$(window).off('click.vb_lb_menu').on('click.vb_lb_menu', function(event) {
				if($(event.target).is('.node-page-layout-builder-form .btn-more')) {
					event.preventDefault();
					$('.block-local-tasks-block').toggleClass('active');
				}
				else if(!$(event.target).parents('.block-local-tasks-block').length) {
					$('.block-local-tasks-block').removeClass('active');
				}
			});
		}
	};
})(jQuery, Drupal);
